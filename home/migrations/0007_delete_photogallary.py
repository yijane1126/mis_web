# Generated by Django 3.1.2 on 2022-05-23 16:43

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0006_auto_20220523_2322'),
    ]

    operations = [
        migrations.DeleteModel(
            name='PhotoGallary',
        ),
    ]
