# Generated by Django 3.1.2 on 2022-05-31 13:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0009_auto_20220525_1726'),
    ]

    operations = [
        migrations.AddField(
            model_name='document',
            name='saliency',
            field=models.FileField(blank=True, upload_to='.'),
        ),
    ]
