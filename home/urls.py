from django.urls import path
from . import views


urlpatterns = [
    path('', views.homepage, name='Home'),
    # path('upload', views.upload, name='Upload'),
    path('upload_multi', views.post_upload_images, name='Upload_multi'),
    path('result', views.result, name='Result'),
]