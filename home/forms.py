from django import forms
import os

class ImageUploadForm(forms.Form):
    image = forms.ImageField()

# class FileFieldForm(forms.Form):
#     file_field = forms.FileField(widget=forms.ClearableFileInput(attrs=
#         {'multiple': True, 'webkitdirectory': True, 'directory': True}))

# class FileFieldForm(forms.Form):
#     file_field = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}))
