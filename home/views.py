from django.shortcuts import render
from django.views import generic
from django.http import HttpResponse
from django.views.generic.edit import FormView
from .forms import ImageUploadForm
from home.models import Document
import os, re, json, base64
from torchvision import models
from torchvision import transforms
from PIL import Image
from django.conf import settings
import os.path, shutil
import numpy as np
import matplotlib.pyplot as plt
import torch
import torch.nn.functional as F
import torchvision, torchvision.transforms
import skimage, skimage.filters
import torchxrayvision as xrv
import pickle
from io import BytesIO
os.environ['KMP_DUPLICATE_LIB_OK']='True'
plt.switch_backend('Agg')
# The root path in this python project
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Model 的 Class
class PneumoniaSeverityNetStonyBrook(torch.nn.Module):
    """
    Trained on Stony Brook data https://doi.org/10.5281/zenodo.4633999
    Radiographic Assessment of Lung Opacity Score Dataset
    """
    def __init__(self):
        super(PneumoniaSeverityNetStonyBrook, self).__init__()
        self.basemodel = xrv.models.DenseNet(weights="all")
        self.basemodel.op_threshs = None
        self.net_geo = self.create_network(os.path.join(BASE_DIR, "weights/mlp_geo.pkl"))
        self.net_opa = self.create_network(os.path.join(BASE_DIR, "weights/mlp_opa.pkl"))        

    def create_network(self, path):
        coefs, intercepts = pickle.load(open(path,"br"))
        layers = []
        for i, (coef, intercept) in enumerate(zip(coefs, intercepts)):
            la = torch.nn.Linear(*coef.shape)
            la.weight.data = torch.from_numpy(coef).T.float()
            la.bias.data = torch.from_numpy(intercept).T.float()
            layers.append(la)
            if i < len(coefs)-1: # if not last layer
                layers.append(torch.nn.ReLU())
                
        return torch.nn.Sequential(*layers)
        
    def forward(self, x):
        
        ret = {}
        ret["feats"] = self.basemodel.features2(x)
        
        ret["geographic_extent"] = self.net_geo(ret["feats"])[0]
        ret["geographic_extent"] = torch.clamp(ret["geographic_extent"],0,8)
        
        ret["opacity"] = self.net_opa(ret["feats"])[0]
        ret["opacity"] = torch.clamp(ret["opacity"],0,8)
        
        return ret

# 模型 inference 的 pipeline，會 return 出來一個 dict output
# output 包括：影像、opacity、geographic_extent
def process(model, img_path, cuda=False):
    
    img = skimage.io.imread(img_path)
    img = xrv.datasets.normalize(img, 255)  

    # Check that images are 2D arrays
    if len(img.shape) > 2:
        img = img[:, :, 0]
    if len(img.shape) < 2:
        print("error, dimension lower than 2 for image")

    # Add color channel
    img = img[None, :, :]                    
    
    transform = torchvision.transforms.Compose([xrv.datasets.XRayCenterCrop(),
                                                xrv.datasets.XRayResizer(224)])

    img = transform(img)
    
    with torch.no_grad():
        img = torch.from_numpy(img).unsqueeze(0)
        if cuda:
            img = img.cuda()
            model = model.cuda()

        outputs = model(img)
    
    outputs["img"] = img
    return outputs

# Create your views here.
def homepage(request):
    context = {

    }
    return render(request, 'home/homepage.html', context)


def _handle_uploaded_file(file):
    """Deal with file upload   
    """
    # Write the file to media folder
    destination = open(os.path.join(BASE_DIR, 'media/{}'.format(file.name)), 'wb+')
    for chunk in file.chunks():
        destination.write(chunk)
    destination.close()

def post_upload_images(request):
    
    dic = {}
    path = []

    if request.method == 'POST':
        
        files = request.FILES.getlist('files')
        
        #清空session
        keys = list(request.session.keys())
        for k in keys:
            del request.session[k]

        #清空upload_files資料夾
        folder = settings.MEDIA_ROOT
        if not os.path.exists(folder):
            os.makedirs(folder)
        for filename in os.listdir(folder):
            file_path = os.path.join(folder, filename)
            try:
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path):
                    shutil.rmtree(file_path)
            except Exception as e:
                print('Failed to delete %s. Reason: %s' % (file_path, e))
        
        for afile in files:
            
            image_bytes = afile.file.read()
            
            encoded_img = base64.b64encode(image_bytes).decode('ascii')
            image_uri = 'data:%s;base64,%s' % ('image/jpeg', encoded_img)
            
            dic[afile] = image_uri #將圖片存入dictionary
            request.session[str(afile)] = image_uri #儲存檔名及原始圖片給result
            
            #存入model中，會上傳到upload_files資料夾
            newdoc = Document(docfile = afile)
            newdoc.save()
            path.append(newdoc.docfile.path) #影像路徑加入list中
            # _handle_uploaded_file(afile)
    
    request.session['path'] = path #儲存圖片路徑給result
    
    context = {
        'dic': dic, #上傳圖片dictionary (key: 檔名, value: 圖片)
    }
    return render(request, 'home/upload_multi.html', context)

def result(request):
    dic = {}
    paths = request.session['path'] #原始圖片路徑

    for path in paths:
        img_name = os.path.basename(path)
        img_path = path
        saliency_path = os.path.join(settings.MEDIA_ROOT, img_name+'_output.png')

        # 定義模型
        model = PneumoniaSeverityNetStonyBrook()

        # 執行 inference
        outputs = process(model, img_path)

        # 定義 opacity 和 geographic_extent 的資料格式
        geo = float(outputs["geographic_extent"].cpu().numpy())
        opa = float(outputs["opacity"].cpu().numpy())

        # 定義影像
        img = outputs["img"]
        img = img.requires_grad_()
        outputs = model(img)
        grads = torch.autograd.grad(outputs["geographic_extent"], img)[0][0][0]
        blurred = skimage.filters.gaussian(grads.detach().cpu().numpy()**2, sigma=(5, 5), truncate=3.5)

        my_dpi = 100
        fig = plt.figure(frameon=False, figsize=(224/my_dpi, 224/my_dpi), dpi=my_dpi)
        ax = plt.Axes(fig, [0., 0., 1., 1.])
        ax.set_axis_off()
        fig.add_axes(ax)
        ax.imshow(img[0][0].detach().cpu().numpy(), cmap="gray", aspect='auto')
        ax.imshow(blurred, alpha=0.5)
        plt.ioff()
        plt.savefig(saliency_path)
        
        with open(saliency_path , "rb") as image_file :
            image_bytes = image_file.read()
            encoded_img = base64.b64encode(image_bytes).decode('ascii')
            image_uri = 'data:%s;base64,%s' % ('image/jpeg', encoded_img)
        
        dic[img_name] = [image_uri, round(geo, 2), round(opa, 2)]

    context = {
        'dic': dic,  #預測結果dictionary (key: 檔名, value: 圖片)
    }
    return render(request, 'home/result.html', context)

def handle_uploaded_file(f):
    with open(f.name, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
